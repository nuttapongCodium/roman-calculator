var last =  function(array, n) {
  if (array == null) 
    return void 0;
  if (n == null) 
     return array[array.length - 1];
  return array.slice(Math.max(array.length - n, 0));  
  };

export function decimalToRoman(num: number): string {
  var digits = String(+num).split(""),
    lastThree = last(digits, 3),
    key = [
      "",
      "C",
      "CC",
      "CCC",
      "CD",
      "D",
      "DC",
      "DCC",
      "DCCC",
      "CM",
      "",
      "X",
      "XX",
      "XXX",
      "XL",
      "L",
      "LX",
      "LXX",
      "LXXX",
      "XC",
      "",
      "I",
      "II",
      "III",
      "IV",
      "V",
      "VI",
      "VII",
      "VIII",
      "IX",
    ],
    roman = "",
    digits = digits.slice(0,-4),
    i = 2
  lastThree.reverse().map((digit: string) => roman = (key[+digit + ((i--) * 10)] || "") + roman);

  return Array(+digits.join("") + 1).join("M") + roman;
}

export function romanToDecimal(string: string): number {
  var str = string.toUpperCase(),
    token = /[MDLV]|C[MD]?|X[CL]?|I[XV]?/g,
    key = {
      M: 1000,
      CM: 900,
      D: 500,
      CD: 400,
      C: 100,
      XC: 90,
      L: 50,
      XL: 40,
      X: 10,
      IX: 9,
      V: 5,
      IV: 4,
      I: 1,
    };
  const romanNumbers = str.match(token);
  const decimalNumbers = romanNumbers.map((romanNumber) => {return key[romanNumber]});
  const decimalNumber = decimalNumbers.reduce((sum, number) => {
    return sum + number
  }, 0);
  return decimalNumber;
}

export function addRoman(str1: string, str2: string) {
  const num1 = romanToDecimal(str1);
  const num2 = romanToDecimal(str2);

  return decimalToRoman(num1 + num2);
}
